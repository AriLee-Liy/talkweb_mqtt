/*
 * Copyright (c) 2021-2021, talkweb 拓维信息 www.talkweb.com.cn.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkweb.iot.mqtt.broker.auth;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dreamlu.iot.mqtt.core.server.auth.IMqttServerAuthHandler;
import net.dreamlu.mica.core.utils.StringUtil;
import org.tio.core.ChannelContext;
import org.tio.core.Node;

/**
 * mqtt tcp websocket 认证
 *
 * @author L.cm
 */
@Slf4j
@RequiredArgsConstructor
public class MqttAuthHandler implements IMqttServerAuthHandler {
	private final MqttAuthProperties mqttAuthProperties;

	@Override
	public boolean authenticate(ChannelContext context, String uniqueId, String clientId, String userName, String password) {
		// 获取客户端信息
		Node clientNode = context.getClientNode();
		log.info("mqtt client node:{} connect auth clientId:{} userName:{} password:{}", clientNode, clientId, userName, password);
		// 客户端认证逻辑实现
		if (StringUtil.isBlank(userName) || StringUtil.isBlank(password)) {
			return false;
		}
		String usernameProp = mqttAuthProperties.getUsername();
		String passwordProp = mqttAuthProperties.getPassword();
		return userName.equals(usernameProp) && password.equals(passwordProp);
	}

}
