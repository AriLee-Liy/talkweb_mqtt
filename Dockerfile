FROM moxm/java:1.8-full

RUN mkdir -p /www/mqtt

WORKDIR /www/mqtt

ARG JAR_FILE=target/talkweb-mqtt.jar

COPY ${JAR_FILE} app.jar

EXPOSE 1883 8083 30001

ENV TZ=Asia/Shanghai JAVA_OPTS="-Xms2048m -Xmx2048m -Djava.security.egd=file:/dev/./urandom"

CMD sleep 60; java -jar app.jar $JAVA_OPTS
